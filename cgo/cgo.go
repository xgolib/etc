package cgo

/*
//#include <stdlib.h> // import C.free()
// #include "cgo.h"
//#include <stdint.h> // for uintptr_t
//
//typedef uintptr_t cGoHandle;
//
//// typedef struct {
////     cGoHandle   handle;
//// 	int         code;
//// } cGoError;
//
//typedef struct {
//    cGoHandle   handle;
//	// cGoError    err;
//	cGoHandle   errHandle;
//} cGoString;
//
//typedef struct {
//    cGoHandle   handle;
//	uint32_t    size;
//	// cGoError    err;
//	cGoHandle   errHandle;
//} cGoBlob;


#include "cgo.h"
#include <stdlib.h> // import C.free()
*/
import "C"
import (
	"runtime/cgo"
	"unsafe"
)

type (
	// Handle interface {
	// 	Value() interface{}
	// }
	// Handle  uintptr
	Handle  = C.cGoHandle
	//Recycler interface {
	//	Recycle()
	//}
	HandleT struct {
		value    interface{}
		pointers []unsafe.Pointer
	}
	StringT struct {
		HandleT
		s string
	}
	BlobT struct {
		HandleT
		b []byte
	}
	// Error  interface{}
	ErrorT struct {
		HandleT
		err  error
		code int
	}
	// CGoError = C.cGoError
)

func NewHandle(v interface{}) Handle {
	htp := &HandleT{value: v}
	return Handle(cgo.NewHandle(htp))
}

func (h *HandleT) CString(s string) *C.char {
	cs := C.CString(s)
	h.pointers = append(h.pointers, unsafe.Pointer(cs))
	return cs
}

func (h *HandleT) CBytes(b []byte) unsafe.Pointer {
	cb := C.CBytes(b)
	h.pointers = append(h.pointers, cb)
	return cb
}

func (h *HandleT) Close() error {
	for _, p := range h.pointers {
		C.free(p)
	}
	return nil
}

//func (h *HandleT) Recycle() {
//	for _, p := range h.pointers {
//		C.free(p)
//	}
//}

func NewString(s string) *StringT {
	sp := &StringT{s: s}
	return sp
}

func (sp *StringT) CString() *C.char {
	return sp.HandleT.CString(sp.s)
}

func NewBlob(b []byte) *BlobT {
	bp := &BlobT{b: b}
	return bp
}

func (bp *BlobT) CBytes() unsafe.Pointer {
	return bp.HandleT.CBytes(bp.b)
}

func NewErrorHandle(err error) Handle {
	// func NewError(err error) C.cGoError {
	return NewErrorHandleWithCode(err, 47)
	//code := 47
	//if err == nil {
	//	code = 0
	//}
	//return NewErrorHandleWithCode(err, code)
}

func NewErrorHandleWithCode(err error, code int) (errH Handle) {
	if err == nil {
		return errH
	}

	goErr := newErrorWithCode(err, code)
	errH =Handle( cgo.NewHandle(goErr))
	return errH
	//return Handle(errH)

	//cgoErr := C.cGoError{
	//	handle: Handle(errH),
	//	code:   C.int(code),
	//}
	//return cgoErr
}

func newErrorWithCode(err error, code int) *ErrorT {
	ep := &ErrorT{err: err, code: code}
	return ep
}

func ValueOf(h Handle) interface{} {
	return cgo.Handle(h).Value()
}

//func ErrorOf(h Handle) *ErrorT {
//	return ValueOf(h).(*ErrorT)
//}

// CError returns the error message as a string
func (ep *ErrorT) CError() *C.char {
	return ep.HandleT.CString(ep.err.Error())
}
