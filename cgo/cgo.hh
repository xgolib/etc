#ifndef GOLIB_CGO_HH
#define GOLIB_CGO_HH

// #include <stdint.h> // for uintptr_t
#include "cgo.h"

// typedef uintptr_t cGoHandle;
class GoHandle {
public:
    GoHandle(cGoHandle h);
//     ~GoHandle();
    cGoHandle Close();
private:
    cGoHandle handle;
};

//	ErrorT struct {
//		HandleT
//		err  error
//		code int
//	}
// typedef struct {
//     cGoHandle   handle;
// //	char*	    msg;
// 	int         code;
// } cGoError;

// class GoError : private GoHandle {
// public:
//     GoError(cGoError err);
//     ~GoError();
//     boolean IsOk();
//     boolean IsErr();
//     int Code();
//     std::string Error();
// private:
//     int code;
// };

class GoException
{
public:
    GoException(cGoHandle errHandle);

    std::string ErrorMessage();
private:
    const std::string errMsg;
};

//	StringT struct {
//		HandleT
//		s string
//	}
// typedef struct {
//     cGoHandle   handle;
// //	char*	    str;
// 	cGoError    err;
// } cGoString;
//class GoString : private GoHandle {
//public:
//    GoString(cGoString s);
//    ~GoString();
//    std::string Str();
//private:
////     GoHandle handle;
////     cGoError err;
//};

//	BlobT struct {
//		HandleT
//		b []byte
//	}
// typedef struct {
//     cGoHandle   handle;
// 	uint32_t    size;
// //	void*       data;
// 	cGoError    err;
// } cGoBlob;
class GoBlob : private GoHandle {
public:
    GoBlob(cGoBlob b);
    ~GoBlob();
    void* Bytes();
    uint32_t Size();
private:
    uint32_t    size;
};

#endif // GOLIB_CGO_HH
