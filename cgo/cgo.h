#ifndef GOLIB_CGO_H
#define GOLIB_CGO_H

#include <stdint.h> // for uintptr_t

typedef uintptr_t cGoHandle;

typedef struct {
    cGoHandle   handle;
	int         code;
} cGoError;

typedef struct {
    cGoHandle   handle;
	cGoError    err;
} cGoString;

typedef struct {
    cGoHandle   handle;
	uint32_t    size;
	cGoError    err;
} cGoBlob;

#endif // GOLIB_CGO_H
