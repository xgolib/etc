package cgo

/*
#include "cgo.h"
*/
import "C"
import (
	"io"
)

// CGO_ErrorMessage returns the error message as a string
//export CGO_ErrorMessage
func CGO_ErrorMessage(errHandle Handle) *C.char {
	return ValueOf(errHandle).(*ErrorT).CError()
}

// CGO_Close recycles the resources bound to the handle
//export CGO_Close
func CGO_Close(handle Handle) (errHandle Handle) {
	rec, ok := ValueOf(handle).(io.Closer)
	if ok {
		err := rec.Close()
		if err != nil {
			errHandle = NewHandle(err)
		}
	}
	return errHandle
}
